function mapObject(obj,fun)
{
    if(typeof obj !=='object')
    {
        return {};
    }


    let newobj={...obj};
    for(let val in obj){
        newobj[val]=fun(val);
    }


return newobj;
}

module.exports=mapObject;