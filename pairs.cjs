function pairs(obj)
{
    if(typeof obj !=='object')
    {
        return [];
    }

let pair=[];
for(let val in obj){
    pair.push([val,obj[val]]);
}
    return pair;
}

module.exports=pairs;