function invert(obj)
{
    if(typeof obj !=='object')
    {
        return {};
    }

   let invertobj={};
   for(let val in obj){
    invertobj[obj[val]]=val;
   }

   return invertobj;

}
module.exports=invert;